.. _quickstart:

Quick-start
===========

To start, at first time you should initialize anji-orm and then load it in the proper mode. To simplify tutorial, we will use *sync* mode. 

.. code-block:: python

    from anij_orm import orm_register

    orm_register.init('rethinkdb://', {})
    orm_register.load()

:code:`rethinkdb://` is URI resource that point to the database, you can use it like simple URL for passing database user, password, host, and port. Protocol, like :code:`rethinkdb` will point to orm which adapted should be chosen to work with the database.

All :code:`anji-orm` logic is based on :py:class:`.Model`, so, to start you need to create a simple model and fill is with :py:mod:`.fields`.

.. code-block:: python

    from anji_orm import Model, StringField, DictField, ListField

    class BaseModel(Model):
    
        _table = 'base_table'

        t1 = StringField()
        t2 = StringField(secondary_index=True)
        t3 = DictField()
        t4 = ListField()

Defining :code:`_table` is important, without it model will be processed like *abstract*. Then, you can just create an instance of this class and send it to the database. Every field has default values, so defining every field not required.

.. code-block:: python

    t1 = BaseModel(t1='2', t2='5')
    t1.send()

To query the database, you should be query used :code:`BaseModel` class:

.. code-block:: python

    query = (BaseModel.t1 == '5') & (BaseModel.t2 < '3')
    for t1 in query.run():
        print(t1)

It's imporant to put query parts in brackets, because in python operator :code:`&` has bigger priority then :code:`==`, so without brackes you will receive something like this :code:`((BaseModel.t1 == '5') & BaseModel.t2 ) < '3'`. As alternative, you can use function syntax:

.. code-block:: python

    query = BaseModel.t1.eq('5').and_(BaseModel.t2.lt('3'))

Record operations
~~~~~~~~~~~~~~~~~

A record has a basic method to provide interaction with the database:

.. code-block:: python

    t1 = BaseModel(t1='2', t2='5')
    t1.send()  # Insert or update record into database
    t1.load()  # Load record from database
    t1.delete()  # Remove record from database
    t1.update({'t1': '2'})  # Update record fields and execute send


Also, a special method exists to receive record via id: 

.. code-block:: python
    
    t1 = BaseModel.get('1')


Query syntax
~~~~~~~~~~~~

Query syntax is very simple and basically based on python magic methods:

.. code-block:: python
    
    query = BaseModel.t1 < '5' # Find records with t1 that lower then '5'
    query = BaseModel.t1 > '5' # Find records with t1 that greather then '5'
    query = BaseModel.t1 <= '5' # Find records with t1 that lower or equal then '5'
    query = BaseModel.t1 >= '5' # Find records with t1 that greather or equal then '5'
    query = BaseModel.t1 == '5' # Find records with t1 that equal '5'
    query = BaseModel.t1 != '5' # Find records with t1 that not equal '5'

Due to python restrictions, some methods can be used only in function style:

.. code-block:: python

    query = BaseModel.t1.one_of('5', '6') # Find records with t1 then equal '5' or '6'
    query = BaseModel.t4.contains('5') # Find records with t4 that contains '5'
    query = BaseModel.t1.match('\d{1,3}') # Find records with t1 that match regex '\d{1,3}'

Query could be joined via :code:`&` or :py:meth:`and_` method:

.. code-block:: python

    query = (BaseModel.t1 < '5') & (BaseModel.t2 > '5')

Every query should be run via :py:meth:`.QueryAst.run`. Or, if you want to check or print query, you can use :py:meth:`.QueryAst.build_query`

Async mode
~~~~~~~~~~

This ORM can be started in async mode, that based on python `asyncio <https://docs.python.org/3/library/asyncio.html>`_.

To start ORM in this mode, just pass :code:`async_mode` to init method.

.. code-block:: python

    from anij_orm import orm_register

    orm_register.init('rethinkdb://', {})
    await orm_register.async_load()

Every method, that interact with database has alternative method with prefix :code:`async_` like :py:meth:`.QueryAst.async_run()` or :py:meth:`.Model.async_load` and similar method.
