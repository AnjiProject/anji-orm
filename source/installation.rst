.. _installation:

Installation
============

You can use ``pip`` to install ``anji-orm``:

.. code-block:: console

    pip install anji-orm

The project is hosted at https://gitlab.com/AnjiProject/anji-orm and can be installed from source:

.. code-block:: console

    git clone https://gitlab.com/AnjiProject/anji-orm
    cd anji-orm
    python setup.py install

After installing :code:`anji-orm`, you can run the unit tests with :code:`pytest`:

.. code-block:: console

    pytest tests/
