.. AnjiORM documentation master file, created by
   sphinx-quickstart on Fri Dec 22 17:43:05 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Anji ORM
========

Powerful ORM for document-oriented NoSQL databases that can be used in sync or async mode!

Supported databases
-------------------

- `RethinkDB <https://www.rethinkdb.com/>`_
- `CouchDB <http://couchdb.apache.org/>`_

Supported features
------------------

- Base document operations
- Nested fields query
- Schema on application level
- Secondary index of field and fields combination
- Automated index policies
- Index autoselected, when database require this like RethinkDB
- Aggregation query
- Group query
- Real-time change subscribing

Unsupported features
--------------------

- Joins and linking between models
- Batch updates
- :code:`OR` statements in query

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   installation
   quickstart


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
