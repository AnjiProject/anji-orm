anji\_orm\.couchdb package
==========================

Submodules
----------

anji\_orm\.couchdb\.executor module
-----------------------------------

.. automodule:: anji_orm.couchdb.executor
    :members:
    :undoc-members:
    :show-inheritance:

anji\_orm\.couchdb\.parser module
---------------------------------

.. automodule:: anji_orm.couchdb.parser
    :members:
    :undoc-members:
    :show-inheritance:

anji\_orm\.couchdb\.register module
-----------------------------------

.. automodule:: anji_orm.couchdb.register
    :members:
    :undoc-members:
    :show-inheritance:

anji\_orm\.couchdb\.utils module
--------------------------------

.. automodule:: anji_orm.couchdb.utils
    :members:
    :undoc-members:
    :show-inheritance:

anji\_orm\.couchdb\.view\_ast module
------------------------------------

.. automodule:: anji_orm.couchdb.view_ast
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: anji_orm.couchdb
    :members:
    :undoc-members:
    :show-inheritance:
