#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""
import sys

from setuptools import setup, find_packages
from itertools import chain

with open('README.rst') as readme_file:
    readme = readme_file.read()

extras_require = {
    'rethinkdb-sync': ["repool-forked>=1.0.0"],
    'rethinkdb-async': ["async-repool>=1.0.0"],
    'couchdb-async': ["aiohttp>=3.2.1,<4"],
    'couchdb-sync': ["requests<=2.21.0", 'sseclient-py~=1.7'],
    'unqlite': ["unqlite~=0.7.1"],
    'file-disk': ['aiofiles~=0.4.0'],
    "minio-sync": ["minio>=5.0.7"],
    "minio-async": ["minio>=5.0.7", "aiohttp>=3.2.1,<4"]
}

extras_require['all'] = list(chain(*extras_require.values()))


install_requires = [
    "humanize~=0.5.1",
    "lazy~=1.3",
    "jsonschema>=2.6.0",
    "aenum~=2.1.2",
    "toolz~=0.9.0",
    "funcsubs>=0.4.0",
]

if sys.version[0:3] == '3.6':
    install_requires.append('idna_ssl>=1.1.0')


setup(
    name='anji-orm',
    version='0.11.7',
    description="RethinkDB based ORM",
    long_description=readme,
    author="Bogdan Gladyshev",
    author_email='siredvin.dark@gmail.com',
    url='https://gitlab.com/AnjiProject/anji-orm',
    packages=find_packages(),
    include_package_data=True,
    install_requires=install_requires,
    license="MIT license",
    zip_safe=False,
    keywords='rethinkdb asyncio orm',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Framework :: AsyncIO',
    ],
    tests_require=[],
    setup_requires=[],
    extras_require=extras_require
)
