ARG PYTHON_VERSION=3.7

FROM python:${PYTHON_VERSION}

RUN pip3 install pipenv && mkdir -p /buildenv

WORKDIR /builenv

COPY "anji_orm/" "/builenv/anji_orm"
ADD ["Pipfile", "Pipfile.lock", "setup.py", "README.rst", "/builenv/"]

RUN pipenv install -d --system