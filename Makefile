lint:
	pylint anji_orm tests
	pycodestyle anji_orm tests
	mypy anji_orm --ignore-missing-imports
inspect:
	pipenv check
	bandit -r anji_orm       
docs:
	sphinx-build -b html ./source ./build
pytest:
	pytest --duration 20 --cov anji_orm --cov-report term-missing tests