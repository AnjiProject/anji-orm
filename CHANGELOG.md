# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.11.7] - 2020-02-15

### Fixed

- Strange bug in rethinkdb query builder

## [0.11.6] - 2020-02-14

### Changed

- `jsonschema` dep limitation

## [0.11.5] - 2019-05-17

### Fixed

- Performance improvements for order_by in rethinkdb

## [0.11.4] - 2019-04-25

### Fixed

- Relations shouldn't used when string formatting

## [0.11.3] - 2019-04-03

### Fixed

- delete handler for file extension

## [0.11.2] - 2019-04-02

### Added

- `sampling_reset` function for filter query

### Fixed

- `AsyncMinio` response wrapper exceptions
- `rethinkdb` indexed order_by conditions

## [0.11.1] - 2019-04-02

### Fixed

- Signal dispatch axis is `class` instead of `object`

## [0.11.0] - 2019-04-01

### Added

- `minio` file extension support

### Changed

- Migrate from `hermes` to `funcsubs` library

## [0.10.9] - 2019-03-11

### Added

- RethinkDB support for python 3.7

### Fixed

- `Model` objects assigned to relation field

## [0.10.8] - 2019-02-11

### Fixed

- Attachments now will be keep after save

## [0.10.7] - 2019-02-07

### Fixed

- orm relation cache

## [0.10.6] - 2019-02-06

### Added

- Ability to provided aliases for models to allow free renaming

## [0.10.5] - 2019-02-01

### Changed

- change aiohttp requiremenets

## [0.10.4] - 2019-01-30

### Fixed

- None usage for FileDictField, again

## [0.10.3] - 2019-01-30

### Fixed

- None usage for FileDictField

## [0.10.2] - 2019-01-30

### Fixed

- Processing files
- Unpack file from dict
- hermes library typing

## [0.10.1] - 2019-01-30

### Fixed

- hermes library in package

## [0.10.0] - 2019-01-30 [YANKED]

### Added

- Extensions framework
- Signals framework
- File extension

## [0.9.2]

### Added

- Simple `filter` function to make filter usage more simple
- Base `ListLinkField` implementation

### Fixed

- `TimedComputeField` expire logic

## [0.9.1]

### Changed

- Async `unqlite` strategy now use thread pool executor

## [0.9.0]

### Added

- `truncate_table` method to cleanup table without delete it.
- `LinkField` and `prefetch_related` to work with
- `unqlite` driver support

### Changed

- Migrate dev env to `Pipfile`

### Fixed

- `order_by` usage after filter without index for rethinkdb
- query adaptation in child subquery

## [0.8.4]

### Fixed

- Sampling merging with `&` logic

## [0.8.3]

### Fixed

- Type checks for complex cases

## [0.8.2]

### Fixed

- Complex query view support for couchdb

## [0.8.1]

### Fixed

- CouchDB aggregation query inque
- CouchDB complex query arguments protection
- CouchDB complex query element access

## [0.8.0]

### Added

- Better child-parent logic support

### Changed

- `_python_info` storing logic. Now it a string to speed up search between child models

## [0.7.7]

### Fixed

- Fix couchdb auth logic

## [0.7.6]

### Added

- Ability to custom cached stored field triggers
- `TimedComputedField`

### Fixed

- Problem with field ownership

## [0.7.5]

### Changed

- datetime adaptation now belong to compitability part

## [0.7.4]

### Fixed

- Problem with `get` logic for couchdb

## [0.7.3]

### Fixed

- Fixed default logic with fetched_meta

## [0.7.2]

### Added

- `first` and `async_first` functions
- python 3.7 partial support (without rethinkdb, driver is not ready yet)

### Fixed

- Enum serialization
- Serialization logic in many places

## [0.7.1]

### Fixed

- Default value settings for `0`, `''` and another False-like objects

## [0.7.0]

### Added

- Base unqlite support

## [0.6.1]

### Fixed

- empty response on `get` request
- Problem with fetch field ...
- Empty count response in couchdb
- Type check for dict
- CouchDB document delete method

## [0.6.0]

### Added

- Better compute field definition
- `VerboseModel` to provide verbose formatting out of the box

### Changed

- Migrated to dataclass similar logic
- Move `SharedEnv` to `orm_register`

### Removed

- `to_describe_dict` method
- Many useless fields from `Field` class
- Every classes for fields except `Field` class

### Fixed

- Requirements now pined

## [0.5.8] - 2018-07-02

### Changed

- `update` statement now has unified output

### Fixed

- CouchDB aiohttp response not closed properly
- `changes` statement for sync couchdb
- `changes` statement for sync rethinkdb

## [0.5.7] - 2018-06-30

### Added

- `and_` method for function syntax
- `update` statement

### Changed

- Internal parser structure now more complex and query structure now based on three types - fitler, transformator and operator.
- Default index policy now `single`

## [0.5.6] - 2018-06-17

### Added

- Complex case for couchdb `change`

## [0.5.5] - 2018-06-17

### Fixed

- Problem with deleted changefeed

### Changed

- `id` now displayed and not service field

## [0.5.4] - 2018-06-17

### Fixed

- `get` provided for rethinkdb

## [0.5.3] - 2018-06-17

### Added

- `changes` statement

### Changed

- Now result will be iterator instead of list

## [0.5.2] - 2018-06-15

### Added

- `group` query syntax

### Changed

- Now all models use `__slots__` variable
- Fields use `__slots__`
- Query ast use `__slots__`

## [0.5.1] - 2018-06-10

### Added

- New `merge` operant
- Ability to execute `~T1.c1` for bool field-
- Aliases for magic method, that used for comparation

## [0.5.0] - 2018-06-06

### Added

- New `QueryTable` ast part
- `limit`, `sample`, `skip` support for `QueryAst`
- `order_by` support for `QueryAst`
- CouchDB integration
- Nested field queries support

### Changed

- Split logic to abstract and real
- **Breaking** many API changes
- Now connection should use connection_uri
- Fields initialization way

### Removed

- Relation fields support (temporary)
- Wrapper function support (really useless)

### Fixed

- `BooleanQueryRow` now has implict cast from both sides

## [0.4.8] - 2018-05-12

### Changed

- Required version of `async-repool` changed from `>=0.1.0` to `>=0.3.0`

## [0.4.8] - 2018-05-10

### Added

- Magic processing of boolean field to avoid `a == True` expressions

## [0.4.7] - 2018-05-04

### Added

- Ability to execute query itself
- `Greedyless` index policy to avoid many useless indexes

## [0.4.6] - 2018-02-07

### Fixed

- Fixed async_send with custom id

## [0.4.5] - 2018-01-27

### Added

- Ability to set custom `id` field to record

## [0.4.4] - 2018-01-25

### Fixed

- `execute` and `async_execute` without fetch work

## [0.4.3] - 2018-01-25

### Fixed

- `contains` query with string or row

## [0.4.2] - 2018-01-24

### Added

- Ability to `query` without fetch

## [0.4.1] - 2018-01-24

### Added

- Ability to execute without fetch

## [0.4.0] - 2018-01-24

### Added

- Migration logic
- Version for schema
- `build_similarity_query` implementation

### Changed

- Now all ast classes become `QueryAst` subclasses

## [0.3.20] - 2018-01-22

### Fixed

- `get_all` and compound index

## [0.3.19] - 2018-01-17

### Added

- function to compare rethinkdb queries (#4)
- ability to fetch dict-like data that not related to orm classes (#5)

### Changed

- query comparation in tests from build to new compare function (#4)

## [0.3.18] - 2018-01-14

### Added

- New build restriction

### Fixed

- between query usage
- Index selection without useless fields

## [0.3.17] - 2018-01-14

### Fixed

- Real value gettting for list link field fix again

## [0.3.15] - 2018-01-14

### Fixed

- Real value getting for list link field

## [0.3.14] - 2018-01-11

### Added

- Ability to use stored and not stored fields

## [0.3.13] - 2018-01-10

### Added

- Ability to make compute function in async mode, will return Future object now.

## [0.3.12] - 2018-01-10

### Added

- Limits to hipotesis lists strategy

### Changed

- More text in query build expections

### Fixed

- Fixed `prettify_value` for tuple classes

## [0.3.11] - 2018-01-09

### Added

- Exception when trying to setup compute field

### Fixed

- Compute field first value logic

## [0.3.10] - 2018-01-09 [YANKED]

### Fixed

- `all` function with `skip` usage

## [0.3.9] - 2018-01-09

### Added

- `sample` function
- `raw_query` function

## [0.3.8] - 2018-01-09

### Added

- `count` function

### Changed

- `all` function logic, now return all by default and accept args like `skip` and `limit`

## [0.3.7] - 2018-01-09

### Added

- Coverage report
- Some new tests for utils

### Changed

- Return to mypy usage
- Modify `ne` behaivor with `isin` and `bound` merge (#2)

### Fixed

- Fix default for any field (#1)
- Bound calculation

## [0.3.6] - 2017-12-29

### Added

- `all` property to simplity query building
- Some crunch to work with LinkField and ListLinkField in async mode

### Fixed

- Fetch execution in async mode

## [0.3.5] - 2017-12-29

### Fixed

- Initial setup list to ListLinkField without get

## [0.3.4] - 2017-12-29

### Fixed

- Setting argument in ListLinkField

## [0.3.3] - 2017-12-28

### Added

- Cache for relation fields
- ListLinkField
- Check between model methods and register mode

### Changed

- LinkField field now store cache in WeakValueDictionary cache 
- Real field value now can be getter via `real-value` method

### Fixed

- complicated property naming
- execute function with result processing
- Log message on fetch problem
- Problem with Enum convertation

## [0.3.2] - 2017-12-27

### Added

- `async_query` function

## [0.3.1] - 2017-12-27

### Added

- New Json field
- ValidableJsonField

### Fixed

- Pylint problem with abstract classes

## [0.3.0] - 2017-12-25

### Added

- More sugar to sugar god
- Wide query syntax support

### Changed

- Remove Pipfile
- Rename RethinkModelMetaclass to ModelMetaclass

## [0.2.1] - 2017-12-20

### Changed

- Rename package to '-'
- Use Pipfile instead of requirements.txt

## [0.2.0] - 2017-12-13

### Added

- Strict typing


## [0.1.7] - 2017-12-11

### Fixed

- Normal dependency usage

## [0.1.6] - 2017-12-11 [YANKED]

### Fixed

- Normal dependency via setup.py

## [0.1.5] - 2017-12-10

### Fixed

- Wrap function usage

## [0.1.4] - 2017-12-10

### Added

- Ability to import RethinkModelMetaclass

## [0.1.2] - 2017-12-10

### Fixed

- Dependency problem

## [0.1.1] - 2017-12-10 [YANKED]

### Added

- Ability to skip database setup when load register

[Unreleased]: https://gitlab.com/AnjiProject/anji-core/compare/v0.11.1...HEAD
[0.11.1]: https://gitlab.com/AnjiProject/anji-core/compare/v0.11.0..v0.11.1
[0.11.0]: https://gitlab.com/AnjiProject/anji-core/compare/v0.10.9..v0.11.0
[0.10.9]: https://gitlab.com/AnjiProject/anji-core/compare/v0.10.8..v0.10.9
[0.10.8]: https://gitlab.com/AnjiProject/anji-core/compare/v0.10.7..v0.10.8
[0.10.7]: https://gitlab.com/AnjiProject/anji-core/compare/v0.10.6..v0.10.7
[0.10.6]: https://gitlab.com/AnjiProject/anji-core/compare/v0.10.5..v0.10.6
[0.10.5]: https://gitlab.com/AnjiProject/anji-core/compare/v0.10.4..v0.10.5
[0.10.4]: https://gitlab.com/AnjiProject/anji-core/compare/v0.10.3..v0.10.4
[0.10.3]: https://gitlab.com/AnjiProject/anji-core/compare/v0.10.2..v0.10.3
[0.10.2]: https://gitlab.com/AnjiProject/anji-core/compare/v0.10.1..v0.10.2
[0.10.1]: https://gitlab.com/AnjiProject/anji-core/compare/v0.10.0..v0.10.1
[0.10.0]: https://gitlab.com/AnjiProject/anji-core/compare/v0.9.2..v0.10.0
[0.9.2]: https://gitlab.com/AnjiProject/anji-core/compare/v0.9.1..v0.9.2
[0.9.1]: https://gitlab.com/AnjiProject/anji-core/compare/v0.9.0..v0.9.1
[0.9.0]: https://gitlab.com/AnjiProject/anji-core/compare/v0.8.4..v0.9.0
[0.8.4]: https://gitlab.com/AnjiProject/anji-core/compare/v0.8.3..v0.8.4
[0.8.3]: https://gitlab.com/AnjiProject/anji-core/compare/v0.8.2..v0.8.3
[0.8.2]: https://gitlab.com/AnjiProject/anji-core/compare/v0.8.1..v0.8.2
[0.8.1]: https://gitlab.com/AnjiProject/anji-core/compare/v0.8.0..v0.8.1
[0.8.0]: https://gitlab.com/AnjiProject/anji-core/compare/v0.7.7..v0.8.0
[0.7.6]: https://gitlab.com/AnjiProject/anji-core/compare/v0.7.6..v0.7.7
[0.7.6]: https://gitlab.com/AnjiProject/anji-core/compare/v0.7.5..v0.7.6
[0.7.5]: https://gitlab.com/AnjiProject/anji-core/compare/v0.7.4..v0.7.5
[0.7.4]: https://gitlab.com/AnjiProject/anji-core/compare/v0.7.3..v0.7.4
[0.7.3]: https://gitlab.com/AnjiProject/anji-core/compare/v0.7.2..v0.7.3
[0.7.2]: https://gitlab.com/AnjiProject/anji-core/compare/v0.7.1..v0.7.2
[0.7.1]: https://gitlab.com/AnjiProject/anji-core/compare/v0.7.0..v0.7.1
[0.7.0]: https://gitlab.com/AnjiProject/anji-core/compare/v0.6.1..v0.7.0
[0.6.1]: https://gitlab.com/AnjiProject/anji-core/compare/v0.6.0..v0.6.1
[0.6.0]: https://gitlab.com/AnjiProject/anji-core/compare/v0.5.8..v0.6.0
[0.5.8]: https://gitlab.com/AnjiProject/anji-core/compare/v0.5.7..v0.5.8
[0.5.7]: https://gitlab.com/AnjiProject/anji-core/compare/v0.5.6..v0.5.7
[0.5.6]: https://gitlab.com/AnjiProject/anji-core/compare/v0.5.5..v0.5.6
[0.5.5]: https://gitlab.com/AnjiProject/anji-core/compare/v0.5.4..v0.5.5
[0.5.4]: https://gitlab.com/AnjiProject/anji-core/compare/v0.5.3..v0.5.4
[0.5.3]: https://gitlab.com/AnjiProject/anji-core/compare/v0.5.2..v0.5.3
[0.5.2]: https://gitlab.com/AnjiProject/anji-core/compare/v0.5.1..v0.5.2
[0.5.1]: https://gitlab.com/AnjiProject/anji-core/compare/v0.5.0..v0.5.1
[0.5.0]: https://gitlab.com/AnjiProject/anji-core/compare/v0.4.9..v0.5.0
[0.4.9]: https://gitlab.com/AnjiProject/anji-core/compare/v0.4.8..v0.4.9
[0.4.8]: https://gitlab.com/AnjiProject/anji-core/compare/v0.4.7..v0.4.8
[0.4.7]: https://gitlab.com/AnjiProject/anji-core/compare/v0.4.6..v0.4.7
[0.4.6]: https://gitlab.com/AnjiProject/anji-core/compare/v0.4.5..v0.4.6
[0.4.5]: https://gitlab.com/AnjiProject/anji-core/compare/v0.4.4..v0.4.5
[0.4.4]: https://gitlab.com/AnjiProject/anji-core/compare/v0.4.3...v0.4.4
[0.4.3]: https://gitlab.com/AnjiProject/anji-core/compare/v0.4.2...v0.4.3
[0.4.2]: https://gitlab.com/AnjiProject/anji-core/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/AnjiProject/anji-core/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.20...v0.4.0
[0.3.20]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.19...v0.3.20
[0.3.19]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.18...v0.3.19
[0.3.18]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.17...v0.3.18
[0.3.17]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.15...v0.3.17
[0.3.15]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.14...v0.3.15
[0.3.14]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.13...v0.3.14
[0.3.13]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.12...v0.3.13
[0.3.12]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.11...v0.3.12
[0.3.11]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.10...v0.3.11
[0.3.10]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.9...v0.3.10
[0.3.9]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.8...v0.3.9
[0.3.8]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.7...v0.3.8
[0.3.7]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.6...v0.3.7
[0.3.6]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.5...v0.3.6
[0.3.5]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.4...v0.3.5
[0.3.4]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.3...v0.3.4
[0.3.3]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.2...v0.3.3
[0.3.2]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.1...v0.3.2
[0.3.1]: https://gitlab.com/AnjiProject/anji-core/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/AnjiProject/anji-core/compare/v0.2.1...v0.3.0
[0.2.1]: https://gitlab.com/AnjiProject/anji-core/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/AnjiProject/anji-core/compare/v0.1.7...v0.2.0
[0.1.7]: https://gitlab.com/AnjiProject/anji-core/compare/v0.1.6...v0.1.7
[0.1.6]: https://gitlab.com/AnjiProject/anji-core/compare/v0.1.5...v0.1.6
[0.1.5]: https://gitlab.com/AnjiProject/anji-core/compare/v0.1.4...v0.1.5
[0.1.4]: https://gitlab.com/AnjiProject/anji-core/compare/v0.1.2...v0.1.4
[0.1.2]: https://gitlab.com/AnjiProject/anji-core/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/AnjiProject/anji-core/compare/v0.1.0...v0.1.1