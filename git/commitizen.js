"use strict";

module.exports = {
  types: [
    { value: 'feat', name: 'feat:     A new feature' },
    { value: 'fix', name: 'fix:      A bug fix' },
    { value: 'docs', name: 'docs:     Documentation only changes' },
    { value: 'style', name: 'style:    Changes that do not affect the meaning of the code\n            (white-space, formatting, missing semi-colons, etc)' },
    { value: 'ref', name: 'ref: A code change that neither fixes a bug nor adds a feature' },
    { value: 'perf', name: 'perf:     A code change that improves performance' },
    { value: 'test', name: 'test:     Adding missing tests' },
    { value: 'chore', name: 'chore:    Changes to the build process or auxiliary tools\n            and libraries such as documentation generation' },
    { value: 'revert', name: 'revert:   Revert to a commit' },
    { value: 'WIP', name: 'WIP:      Work in progress' }
  ],
  scopes: [
    { name: "query" },
    { name: "couchdb" },
    { name: "rethinkdb" },
    { name: "uqlite" },
    { name: "core" },
    { name: "file" },
  ],
  allowCustomScopes: true,
  scopeOverrides: {
    chore: [
      {name: 'git hooks'},
      {name: 'tests'},
      {name: 'linters'},
      {name: 'scripts'},
      {name: 'docker images'},
      {name: 'build-system'}
    ],
    docs: [
      {name: "changelog"}
    ]
  },
  messages: {
    footer: "List any RELATED ISSUES by this change (optional). E.g.: #1, #5:\n"
  },
  footerPrefix: "RELATED ISSUES:"
};